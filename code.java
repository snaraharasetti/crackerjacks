//My part
//gathering the information, implementing the third combo box design and functionality, layout design, integrating the code
import java.awt.*;
import java.awt.event.*;
import java.awt.Container;
import java.awt.event.MouseEvent;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.border.*;
import javax.swing.ImageIcon;


import java.net.*;

//
public class GSUSmartMap  {
    public static void main(String args[])throws Exception {
        JFrame frame = new GSU_Frame();
        frame.setSize(1150, 780);
        frame.setResizable(false);
        frame.setVisible(true);
    }
}

class GSU_Frame extends JFrame
{
	
	//data in the 4 combo boxes
	
     String imagePath="C:/Users/sreevidya/Downloads/wsp project/windowing programming assignments/Term Project/Project/image/";
     String labels_1[] = {
            "------------------------ Select This Combobox ------------------------",
            "1. Dahlberg Hall(previously Alumni Hall)(ALUM)",
            "2. Arts & Humanities (ARTS)",
            "3. Classrooom South (CLSO)",
            "4. J. Mack Robinson College of Business (RCB)",
            "5. College of Education (COE)",
            "6. College of Law (LAW)",
            "7. Bennet A. Brown Commerce Building (BBCOM)",
            "8. COURTLAND NORTH (COURTN)",
            "9. DAHLBERG HALL (DBERG)",
            "10. 148 EDGEWOOD (148EDG) ",
            "11. FRESHMAN HALL (FRHALL)",
            "12. GENERAL CLASSROOM (GCB)",
            "13. HAAS-HOWELL BUILDING (HAAS)",
            "14. INTEREST HOUSING (INTHOU)",
            "15. KELL HALL (KELL) ",
            "16. HELEN M. ADERHOLD LEARNING CENTER (ADHOLD)",
            "17. LIBRARY NORTH (LIBNO)",
            "18. LIBRARY SOUTH (LIBSO) ",
            "19. NATURAL SCIENCE CENTER (NSC)",
            "20. ONE PARK PLACE (1PP)",
        };
     String labels_2[] = {
            "------------------------ Select This Combobox ------------------------",
            "21. 34 PEACHTREE ST. BUILDING (34PTRE)",
            "22. 75 PIEDMONT (75PIED)",
            "23. PIEDMONT NORTH",
            "24. RIALTO CENTER FOR THE ARTS (RIALTO)",
            "25. SCIENCE ANNEX (SCIANX)",
            "26. PARKER H. PETIT SCIENCE CENTER (PETIT)",
            "27. ANDREW YOUNG SCHOOL OF POLICY STUDIES (AYSPS)",
            "28. SCULPTURE STUDIO (SCULP)",
            "29. SPARKS HALL (SPARKS)",
            "30. SPORTS ANNEX (SPTANX)",
            "31. SPORTS ARENA (ARENA)",
            "32. STANDARD BUILDING (STAND)",
            "33. STUDENT CENTER (STUCTR)",
            "34. STUDENT RECREATION CENTER (REC)",
            "35. TEN PARK PLACE (10PP)",
            "36. UNIVERSITY BOOKSTORE (BOOK)",
            "37. UNIVERSITY CENTER (UCTR)",
            "38. UNIVERSITY COMMONS (COMMON)",
            "39. UNIVERSITY LOFTS (LOFTS)",
            "40. URBAN LIFE BUILDING (URBAN)"
        };
     String labels_info[] = {
            "------------------------ Select This Combobox ------------------------",
            "Original",
            "ATM",
            "Shuttle Routes",
            "Parking",
            "Copy Machine Locations",
            "PantherCard Office"
        };
     String labels_link[] = {
            "------------------------ Select This Combobox ------------------------",
            "1. Georgia State University Homepage",
            "2. Georgia State University Calendar",
            "3. Mail Services Quick Reference Guide",
            "4. Forms and Publications",
            "5. University Bookstore",
            "6. Auxiliary and Support Services",
            "7. PantherDining",
            "8. Google"
        };
    String contents_info_1[] = {
            ""
     ,"Dahlberg Hall (previously Alumni Hall) is home to university Administration and the Alumni Association. It also includes the Welcome Center, the official reception area for visitors to the University."
     ,"This recently constructed facility houses many general purpose classrooms and lecture halls with some office space. "       
    ,"The building was constructed in 1966 and houses numerous classrooms for various departments.The building was constructed in 1966 and houses numerous classrooms for various departments."
    ,"The largest business school in the South and part of a major research institution, the J. Mack Robinson College of Business at Georgia State University is located in Atlanta, an epicenter of business. "
    ,"The College of Education at Georgia State University is known for its outstanding programs in education, counseling and sports-related fields. "
    ,"Georgia State University College of Law offers a dynamic environment for studying law and preparing to enter the legal profession."
    ,"This building houses Information Systems & Technology , Facilities Management Services, RCB's Center for Ethics and Corporate Responsibility, The Commerce Club and the Atlanta Press Club."
    ,"This building houses the ROTC units and several maintenance departments."
    ,"This building is unknown"
    ,"This building is unknown"
    ,"Freshman Hall is able to accommodate 325 to 350 students in two-bedroom units with a shared bathroom. Each floor has lounge space, and a dining hall - Georgia State's first cafeteria-style facility - is located on the ground floor."
    ,"General Classroom Building houses the departments of Political Science, English, Sociology, and Modern and Classical Languages, as well as the offices of the College of Arts and Sciences. "
    ,"Haas-Howell houses the School of Music. Constructed in 1920 and occupied by the university since 1994, it contains School of Music administrative and faculty offices, recording studios, a state-of-the-art media and technology center and classrooms. "
    ,"Special Interest Student Housing consists of nine three-story town homes with varying capacity, for a total of 145 beds. Each town home also includes a student activity center and a laundry facility. The area houses Georgia State’s 24 Greek organizations."
    ,"Constructed in 1920, Kell Hall was originally a parking garage, and parallel ramps still connect the floors. Chemistry and Biology research labs, offices, and support facilities are housed here, as well as offices for the Center for Behavioral Neuroscience."
    ,"This recently constructed facility houses many general purpose classrooms and lecture halls with some office space. Classrooms are spacious and equipped with current the latest in audio-visual and data technology."
    ,"The recently renovated Georgia State University Library provides leadership in accessing and using information for Georgia's urban research university."
    ,"Library South houses the Media Center, containing over 24,000 audio, video, and multimedia materials and two group media rooms on the first floor."
    ,"This building holds classrooms and teaching labs for several science departments. In addition, it houses the main offices of the Departments of Chemistry and Physics and Astronomy"
    ,"Houses departments of Human Resources and University Relations.  Also, on the side of the building, bottom floor, is one location for the University Police Department."
    };   
    String contents_info_2[] = {
            ""
    ,"34 Peachtree is home to the departments of Applied Linguistics (12th floor), Computer Science (14th floor), Philosophy (11th floor),Religious Studies (11th Floor), History (20th Floor), and The Middle East Institute (20th Floor)."
    ,"Georgia State University purchased the Citizens Trust Bank building site at 75 Piedmont Ave. The building is used for university office space, freeing space in other buildings for classroom use."
    ,"AYSPS Research Support collects selected literature and data sets on fiscal policy and economic issues to support ongoing research projects of the faculty and its research centers. Guides for searching regional, national, and international policy issues are available."
    ,"An anchor for the revitalization of the historic Fairlie-Poplar district in downtown Atlanta"
    ,"The spaces in this building are assigned to Chemistry, Biology, Physics, and Astronomy."
    ,"The science center houses programs in biology, chemistry, nutrition, physical and respiratory therapies, public health and the Neuroscience Institute."
    ,"The Andrew Young School's mission is to create and disseminate knowledge and analytical methods that are highly valued by policy makers and leaders in the public, nonprofit, and business worlds"
    ,"The center for the University's sculpture program, the studio includes a foundry that becomes the focus of great excitement and attention during the annual holiday iron pour."
    ,"Sparks Hall houses the Undergraduate Admissions, the Student Advisement Center, and the One Stop Shop. It also primarily houses classrooms and computer laboratory space. The building directly connects on the interior with Kell Hall."
    ,"Sports facilities include the annex and the arena. Athletic teams at Georgia State include football, baseball, basketball, cross-country, golf, soccer, tennis, and track & field for men and basketball, cross-country, golf, soccer, softball, tennis, track & field, and volleyball for women."
    ,"The Sports Arena serves as headquarters for Georgia State Athletics, housing offices for head coaches in all sports as well as the athletic director's office and various administrative departments. The student-athlete development center and the strength and conditioning center are also located in the Sports Arena."
    ,"Constructed in 1923 and occupied by the university in 1984, this building is home to School of Music classrooms, faculty offices, rehearsal rooms for small instrumental ensembles, student and faculty lounges, and recording studios."
    ,"The student center includes facilities and services designed to meet the needs of the campus community. These include meeting spaces, lounges, food courts, and facilities such as the state-of-the-art Digital Aquarium. "
    ,"The state-of-the-art Student Recreation Center contains a climbing wall, racquetball and squash courts, a basketball court, a game room with billiard tables, board games, table tennis and darts, extensive work-out areas for cardio and weight training, a lap and leisure pool, and a spa and dry sauna."
    ,"Also known as the Thorton Building, Ten Park Place houses a variety of programs from the College of Arts and Sciences. The Office of International Affairs, Georgia State's central international education office with information on the study abroad programs for students, is also located in 10 Park Place."
    ,"The bookstore is the official headquarters for textbooks, class supplies, Georgia State University insignia merchandise, gift items, general books, computers, software, and snacks. Our convenient one-stop shopping facility provides numerous advantages for students, faculty, and staff on the go."
    ,"The University Center was built in 1963 and is home to the Honors Program, Cinefest, fraternity and sorority offices, and food services."
    ,"One of the largest residence halls in the country, The Commons, a 4.2-acre complex of four apartment buildings ranging from 8 to 15 stories houses approximately 2,000 Georgia State University students, primarily freshmen, with designated space for upper-class students."
    ,"The Lofts houses approximately 450 residents. Students can enjoy fully furnished loft-style apartments that blend urban style with modern-day conveniences. "
    ,"Constructed in 1974, the 12-story Urban Life Building was the tallest building owned by Georgia State University until the early 1990's. With a total gross area of 338,020 square feet, it is the largest academic instructional facility on campus. "
    };   
    // combo box3 functionality
    
    int x,y,z,a,b,c,rect,i;
    boolean area=false;
    String image_address = imagePath+"GSUCampusMap2011-Original.jpg";
    JLabel picture;
    JComboBox g_comboBox_info = new JComboBox(labels_info);
    Image img;
// GSU Main Image Panel    
    class MainImage extends JPanel{
        int x,y;
        public MainImage() {
            setLayout(null);
      
            g_comboBox_info.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            String item = (String)g_comboBox_info.getSelectedItem();
                            updateLabel(item);
                        }
                    }
            );   

            picture = new JLabel();
            picture.setBorder(BorderFactory.createEmptyBorder(-40,0,0,0));
            ImageIcon icon = new ImageIcon(image_address);
            img = icon.getImage();
 
            addMouseMotionListener(new mouseover());
            addMouseListener(new clicked());
        }
                
    public void updateLabel(String name) {
            java.net.URL imgURL = MainImage.class.getResource(image_address);
            image_address = 
                    imagePath+"GSUCampusMap2011-"+name+".jpg";
            ImageIcon icon = new ImageIcon(image_address);
            if (name.equals("Original")){
                picture.setVisible(false);
            }
            else{
                picture.setVisible(true);
                picture.setIcon(icon);
                if (icon != null) {
                    picture.setText(null);
                } else {
                    picture.setText("Image not found");
                }
            }
        }

 // Priyanka's part    
    class clicked extends MouseAdapter{
            public void mouseClicked(MouseEvent e){
                if(i==0)
                    System.out.println("Not Selected !!");
                else
                    g_TextPane(i);
            }
     } 
    class mouseover extends MouseAdapter{
        public void mouseMoved(MouseEvent e) {
                x=e.getX();
                y=e.getY();
                repaint();
            }
        }
    public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.BLACK);
            g.drawImage(img,0, 0, null);
            a=x+5;
            b=y-10;
            c=20;
            z=y-25;
            rect=320;
            area=false;
            if ((x>322 && x<342) && (y>398 && y<418)){
                area=true;
                i=1;
            }
            else if ((x>260 && x<280) && (y>488 && y<508)) {
                area=true;
                i=2;
            }
            else if ((x>103 && x<123) && (y>309 && y<329)) {
                area=true;
                i=3;
            }
            else if ((x>161 && x<181) && (y>428 && y<448)) {
                area=true;
                i=4;
            }
            else if ((x>385 && x<405) && (y>546 && y<566)) {
                area=true;
                i=5;
            }
            else if ((x>77 && x<97) && (y>297 && y<317)) {
                area=true;
                i=6;
            }
            else if ((x>287 && x<307) && (y>580 && y<600)) {
                area=true;
                i=7;
            }
            else if ((x>303 && x<323) && (y>554 && y<574)) {
                area=true;
                i=8;
            }
            else if ((x>447 && x<467) && (y>398 && y<418)) {
                area=true;
                rect=220;
                i=9;
            }
            else if ((x>514 && x<534) && (y>342 && y<362)) {
                area=true;
                rect=220;
                x=x-rect;
                a=a-rect;
                i=10;
            }
            else if ((x>563 && x<583) && (y>342 && y<362)) {
                area=true;
                rect=220;
                x=x-rect;
                a=a-rect;
                i=11;
            }
            else if ((x>278 && x<298) && (y>451 && y<471)) {
                area=true;
                i=12;
            }
            else if ((x>113 && x<133) && (y>189 && y<209)) {
                area=true;
                i=13;
            }
            else if ((x>542 && x<562) && (y>344 && y<364)) {
                area=true;
                rect=220;
                x=x-rect;
                a=a-rect;
                i=14;
            }
            else if ((x>308 && x<328) && (y>423 && y<443)) {
                area=true;
                i=15;
            }
            else if ((x>153 && x<173) && (y>213 && y<233)) {
                area=true;
                rect=350;
                i=16;
            }
            else if ((x>316 && x<336) && (y>486 && y<506)) {
                area=true;
                i=17;
            }
            else if ((x>289 && x<309) && (y>524 && y<544)) {
                area=true;
                i=18;
            }
            else if ((x>218 && x<238) && (y>411 && y<431)) {
                area=true;
                i=19;
            }
            else if ((x>159 && x<179) && (y>374 && y<394)) {
                area=true;
                i=20;
            }
            else if ((x>121 && x<141) && (y>330 && y<350)) {
                area=true;
                i=21;
            }
            else if ((x>604 && x<624) && (y>193 && y<213)) {
                area=true;
                rect=220;
                x=x-rect;
                a=a-rect;
                i=22;
            }
            else if ((x>621 && x<641) && (y>33 && y<53)) {
                area=true;
                rect=160;
                x=x-rect;
                a=a-rect;
                i=23;
            }
            else if ((x>131 && x<151) && (y>175 && y<195)) {
                area=true;
                i=24;
            }
            else if ((x>261 && x<281) && (y>416 && y<436)) {
                area=true;
                i=25;
            }
            else if ((x>386 && x<406) && (y>619 && y<639)) {
                area=true;
                i=26;
            }
            else if ((x>89 && x<109) && (y>369 && y<389)) {
                area=true;
                rect=350;
                i=27;
            }
            else if ((x>613 && x<633) && (y>334 && y<354)) {
                area=true;
                rect=250;
                x=x-rect;
                a=a-rect;
                i=28;
            }
            else if ((x>375 && x<395) && (y>431 && y<451)) {
                area=true;
                rect=250;
                i=29;
            }
            else if ((x>355 && x<375) && (y>593 && y<613)) {
                area=true;
                rect=250;
                i=30;
            }
            else if ((x>329 && x<349) && (y>568 && y<588)) {
                area=true;
                rect=250;
                i=31;
            }
            else if ((x>113 && x<133) && (y>160 && y<180)) {
                area=true;
                rect=250;
                i=32;
            }
            else if ((x>419 && x<439) && (y>491 && y<511)) {
                area=true;
                rect=250;
                i=33;
            }
            else if ((x>451 && x<471) && (y>536 && y<556)) {
                area=true;
                rect=260;
                i=34;
            }
            else if ((x>192 && x<212) && (y>382 && y<402)) {
                area=true;
                rect=250;
                i=35;
            }
            else if ((x>369 && x<389) && (y>486 && y<506)) {
                area=true;
                rect=250;
                i=36;
            }
            else if ((x>345 && x<365) && (y>513 && y<533)) {
                area=true;
                rect=250;
                i=37;
            }
            else if ((x>628 && x<648) && (y>89 && y<109)) {
                area=true;
                rect=250;
                x=x-rect;
                a=a-rect;
                i=38;
            }
            else if ((x>487 && x<509) && (y>373 && y<393)) {
                area=true;
                rect=200;
                i=39;
            }
            else if ((x>405 && x<425) && (y>531 && y<551)) {
                area=true;
                rect=250;
                i=40;
            }
            else 
                i=0;
            if(area==true){
                super.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                g.setColor(Color.BLUE);
                g.fill3DRect(x, z, rect, c, true);
                g.setColor(Color.BLACK);
                g.draw3DRect(x, z, rect, c, true);
                g.setColor(Color.WHITE);
                if(i>=0 && i<21)    
                    g.drawString(labels_1[i], a, b);
                else 
                    g.drawString(labels_2[i-20], a, b);
            }
            else
                super.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
}     
    private ImageIcon scale(Image src, double scale) {
            int width = (int)(scale*src.getWidth(this));
            int height = (int)(scale*src.getHeight(this));
            int type = BufferedImage.TYPE_INT_RGB;
            BufferedImage dst = new BufferedImage(width, height, type);
            Graphics2D g2 = dst.createGraphics();
            g2.drawImage(src, 0, 0, width, height, this);
            g2.dispose();
            return new ImageIcon(dst);
        }
    
    // Smriti's part
     public void g_TextPane(int x){
        JFrame frame = new JFrame("GSU Smart Map");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        StyleContext context = new StyleContext();
        StyledDocument document = new DefaultStyledDocument(context);

        Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setAlignment(style, StyleConstants.ALIGN_LEFT);
        StyleConstants.setFontSize(style, 14);
        StyleConstants.setSpaceAbove(style, 4);
        StyleConstants.setSpaceBelow(style, 4);

        SimpleAttributeSet attributes = new SimpleAttributeSet();
        StyleConstants.setBold(attributes, true);
        StyleConstants.setItalic(attributes, true);
        StyleConstants.setFontSize(attributes, 15);
 
        try {
            if(x>=0 && x<21){
                document.insertString(document.getLength(), labels_1[x]+"\n", attributes);
                document.insertString(document.getLength(), contents_info_1[x]+"\n", style);
            }
            else{
                document.insertString(document.getLength(), labels_2[x-20]+"\n", attributes);
                document.insertString(document.getLength(), contents_info_2[x-20]+"\n", style);
            }    
        } catch (BadLocationException badLocationException) {
          System.err.println("Oops");
        }
        Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);

        ImageIcon icon = new ImageIcon(imagePath+"GSU.png");
        JLabel label = new JLabel(icon);
        StyleConstants.setComponent(labelStyle, label);
        try {
          document.insertString(document.getLength(), "Ignored", labelStyle);
        } catch (BadLocationException badLocationException) {
          System.err.println("Oops");
        }
        
        JTextPane textPane = new JTextPane(document);
        textPane.setEditable(false);
        frame.add(textPane, BorderLayout.CENTER);
        frame.setSize(330, 450);
        frame.setResizable(false);
        frame.setVisible(true);
     }  
    public GSU_Frame() throws Exception{
        super("GSU Smart Map");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JComboBox g_comboBox1 = new JComboBox(labels_1);
        final JComboBox g_comboBox2 = new JComboBox(labels_2);
        final JComboBox g_comboBox_link = new JComboBox(labels_link);     
        g_comboBox1.setBackground(Color.white);
        g_comboBox2.setBackground(Color.white);
        g_comboBox_link.setBackground(Color.white);
        g_comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int k = g_comboBox1.getSelectedIndex();
                if (k==0)
                    System.out.println("Not Selected !!");
                else
                    g_TextPane(k);
            }
        }
        );       
        g_comboBox2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int k = g_comboBox2.getSelectedIndex();
                if (k==0)
                    System.out.println("Not Selected !!");
                else
                    g_TextPane(k+20);
            }
        }
        );              
        final URI uri_gsu = new URI("http://www.gsu.edu");
        final URI uri_google = new URI("http://www.google.com");
        final URI uri_gsu_cal = new URI("http://calendar.gsu.edu/calendar/EventList.aspx?fromdate=10/25/2012&todate=11/7/2012&view=DateTime");
        final URI uri_gsu_mail = new URI("http://www.gsu.edu/images/AuxiliaryImages/QuickReference.pdf");
        final URI uri_gsu_form = new URI("http://www.gsu.edu/auxiliary/26263.html");
        final URI uri_gsu_book = new URI("http://www.bkstr.com/webapp/wcs/stores/servlet/AboutBuybackView?langId=-1&catalogId=10001&storeId=10489&demoKey=d");
        final URI uri_gsu_people = new URI("http://www.gsu.edu/auxiliary/31564.html");
        final URI uri_gsu_pantherDining = new URI("http://gsu.edu/pantherdining/");
        g_comboBox_link.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
                   if (Desktop.isDesktopSupported()) {
                            Desktop desktop = Desktop.getDesktop();
                            try {
                                if(g_comboBox_link.getSelectedIndex()==1){
                                    desktop.browse(uri_gsu);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==2){
                                    desktop.browse(uri_gsu_cal);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==3){
                                    desktop.browse(uri_gsu_mail);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==4){
                                    desktop.browse(uri_gsu_form);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==5){
                                    desktop.browse(uri_gsu_book);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==6){
                                    desktop.browse(uri_gsu_people);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==7){
                                    desktop.browse(uri_gsu_pantherDining);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==8){
                                    desktop.browse(uri_google);
                                }
                                else
                                   System.out.println("Not Selected !!"); 
                            } catch (Exception ex) {
                            }
                    } 
            }
        });        
//My part
        Border empty0  = BorderFactory.createEmptyBorder(10,10,10,10);
        Border empty1  = BorderFactory.createEmptyBorder(10,10,10,10);
        Border empty2  = BorderFactory.createEmptyBorder(10,10,10,10);
        Border empty3  = BorderFactory.createEmptyBorder(10,10,10,10);
        Border empty6  = BorderFactory.createEmptyBorder(10,10,10,10);
        TitledBorder title0 = BorderFactory.createTitledBorder("Campus Area Number 1-20");
        TitledBorder title1 = BorderFactory.createTitledBorder("Campus Area Number 21-40");
        TitledBorder title2 = BorderFactory.createTitledBorder("Campus Information");
        TitledBorder title3 = BorderFactory.createTitledBorder("Weather");
        TitledBorder title6 = BorderFactory.createTitledBorder("Website Link");
        Border compound0 = BorderFactory.createCompoundBorder(title0, empty0);
        Border compound1 = BorderFactory.createCompoundBorder(title1, empty1);
        Border compound2 = BorderFactory.createCompoundBorder(title2, empty2);
        Border compound3 = BorderFactory.createCompoundBorder(title3, empty3);
        Border compound6 = BorderFactory.createCompoundBorder(title6, empty6);
        
        g_comboBox1.setBorder(compound0);
        g_comboBox2.setBorder(compound1);
        g_comboBox_info.setBorder(compound2);
        g_comboBox_link.setBorder(compound6);
        
        //adding the combo coxes into the panel 
        JPanel panel = new JPanel();
        LayoutManager layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);
        panel.add(g_comboBox1);
        panel.add(g_comboBox2);
        panel.add(g_comboBox_info);
        panel.add(g_comboBox_link);
        
        //setting the layout
        JComponent newContentPane = new MainImage();
        setContentPane(newContentPane);
        Container content = getContentPane();
        content.setLayout(new BorderLayout());
        content.add(picture, BorderLayout.CENTER);
        content.add(panel, BorderLayout.EAST);
     }
    }