public void g_TextPane(int x){
        JFrame frame = new JFrame("GSU Smart Map");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        StyleContext context = new StyleContext();
        StyledDocument document = new DefaultStyledDocument(context);

        Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setAlignment(style, StyleConstants.ALIGN_LEFT);
        StyleConstants.setFontSize(style, 14);
        StyleConstants.setSpaceAbove(style, 4);
        StyleConstants.setSpaceBelow(style, 4);

        SimpleAttributeSet attributes = new SimpleAttributeSet();
        StyleConstants.setBold(attributes, true);
        StyleConstants.setItalic(attributes, true);
        StyleConstants.setFontSize(attributes, 15);

        // Insert content
        try {
            if(x>=0 && x<21){
                document.insertString(document.getLength(),
labels_1[x]+"\n", attributes);
                document.insertString(document.getLength(),
contents_info_1[x]+"\n", style);
            }
            else{
                document.insertString(document.getLength(),
labels_2[x-20]+"\n", attributes);
                document.insertString(document.getLength(),
contents_info_2[x-20]+"\n", style);
            }
        } catch (BadLocationException badLocationException) {
          System.err.println("Oops");
        }

        // Third style for icon/component
        Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);

        ImageIcon icon = new ImageIcon(imagePath+"GSU.png");
        JLabel label = new JLabel(icon);
        StyleConstants.setComponent(labelStyle, label);

        // Insert content
        try {
          document.insertString(document.getLength(), "Ignored", labelStyle);
        } catch (BadLocationException badLocationException) {
          System.err.println("Oops");
        }

        JTextPane textPane = new JTextPane(document);
        textPane.setEditable(false);

        //combo 1&2 end

        frame.add(textPane, BorderLayout.CENTER);
        frame.setSize(330, 450);
        frame.setResizable(false);
        frame.setVisible(true);
     }

// GSU Main Source Start

    public GSU_Frame() throws Exception{
        super("GSU Smart Map");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        String condition = null;
//        String temp = null;
//        String humidity = null;
//        String wind = null;
//        String icon = null;

        final JComboBox g_comboBox1 = new JComboBox(labels_1);
        final JComboBox g_comboBox2 = new JComboBox(labels_2);
        final JComboBox g_comboBox_link = new JComboBox(labels_link);

// Combobox Start
        g_comboBox1.setBackground(Color.white);
        g_comboBox2.setBackground(Color.white);
        g_comboBox_link.setBackground(Color.white);
        g_comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int k = g_comboBox1.getSelectedIndex();
                if (k==0)
                    System.out.println("Not Selected !!");
                else
                    g_TextPane(k);
            }
        }
        );
        g_comboBox2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int k = g_comboBox2.getSelectedIndex();
                if (k==0)
                    System.out.println("Not Selected !!");
                else
                    g_TextPane(k+20);
            }
        }
        );

// Website Start
        final URI uri_gsu = new URI("http://www.gsu.edu");
        final URI uri_google = new URI("http://www.google.com");
        final URI uri_gsu_cal = new
URI("http://calendar.gsu.edu/calendar/EventList.aspx?fromdate=10/25/2012&todate=11/7/2012&view=DateTime");
        final URI uri_gsu_mail = new
URI("http://www.gsu.edu/images/AuxiliaryImages/QuickReference.pdf");
        final URI uri_gsu_form = new
URI("http://www.gsu.edu/auxiliary/26263.html");
        final URI uri_gsu_book = new
URI("http://www.bkstr.com/webapp/wcs/stores/servlet/AboutBuybackView?langId=-1&catalogId=10001&storeId=10489&demoKey=d");
        final URI uri_gsu_people = new
URI("http://www.gsu.edu/auxiliary/31564.html");
        final URI uri_gsu_pantherDining = new
URI("http://gsu.edu/pantherdining/");
        g_comboBox_link.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
                   if (Desktop.isDesktopSupported()) {
                            Desktop desktop = Desktop.getDesktop();
                            try {
                                if(g_comboBox_link.getSelectedIndex()==1){
                                    desktop.browse(uri_gsu);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==2){
                                    desktop.browse(uri_gsu_cal);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==3){
                                    desktop.browse(uri_gsu_mail);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==4){
                                    desktop.browse(uri_gsu_form);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==5){
                                    desktop.browse(uri_gsu_book);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==6){
                                    desktop.browse(uri_gsu_people);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==7){
                                    desktop.browse(uri_gsu_pantherDining);
                                }
                                else if(g_comboBox_link.getSelectedIndex()==8){
                                    desktop.browse(uri_google);
                                }
                                else
                                   System.out.println("Not Selected !!");
                            } catch (Exception ex) {
                            }
                    }
            }
        });